import java.util.Scanner;

public class ContaTerminalTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ContaTerminal contaTerminal = new ContaTerminal();

        System.out.println("Digite seu nome:");
        contaTerminal.setNomeCliente(scanner.nextLine());

        System.out.println("Digite seu numero de conta:");
        contaTerminal.setNumero(scanner.nextInt());
        scanner.nextLine();

        System.out.println("Digite a sua agencia:");
        contaTerminal.setAgencia(scanner.nextLine());

        System.out.println("Digite seu saldo:");
        contaTerminal.setSaldo(scanner.nextDouble());

        System.out.println(contaTerminal);


    }
}
